const express = require('express');
const fs = require('fs');
const moment = require('moment');

const router = express.Router();
const path = './messages';

const folderReading =  () => {
    let filesNames =[];
    //  fs.readdir(path, (err, files) => {
    //     files.forEach(file => {
    //         console.log(path + '/' + file);
    //         filesNames.push(path + '/' + file);
    //     });
    // });
    filesNames =  fs.readdirSync(path).map(fileName => {
        return path + '/' + fileName;
    });
    return filesNames;
};

router.get('/', (req, res) => {
    let result = [];
    let filesNames = folderReading().reverse();
    if(filesNames.length > 0){
        let length = 0;
        if(filesNames.length > 5){
            length = 5;
        }
        else{
            length = filesNames.length;
        }
        for(let i = 0; i < length; i++){
            const data = fs.readFileSync(filesNames[i]);
            result.push(JSON.parse(data));
        }
        res.send(result);
    }
    else{
        res.send("No messages");
    }
});

router.post('/', (req, res) => {
    const date = moment().format('YYYY-MM-DDTHH.mm.ss');
    console.log(req.body);
    const message = {
        ...req.body,
        datetime: date
    };
    fs.writeFile(path + '/' + date + '.txt', JSON.stringify(message), (err) => {
        if (err) {
            console.error(err);
        } else {
            res.send(JSON.stringify(message));
        }
    })
});

module.exports = router;